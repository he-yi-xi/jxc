package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    // 查询供应商列表
    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> list(Integer page,Integer rows,String supplierName){
        return supplierService.getSupplierList(page,rows,supplierName);
    }

    // 添加供应商
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO saveSupplier(Supplier supplier, HttpServletRequest request){
        if (supplier == null){
            return new ServiceVO(101, "请求失败");
        }
        // 获取supplierId
        String supplierId = request.getParameter("supplierId");
        if (StringUtils.isEmpty(supplierId)){
            supplierService.saveSupplier(supplier);
        }else {
            // 更新
            supplierService.updateSupplier(supplier);
        }
        return new ServiceVO(100, "请求成功");
    }

    // 删除供应商
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(@Param("ids") String[] ids){
        System.out.println(ids);
        List<String> stringList = Arrays.asList(ids);
        supplierService.delete(stringList);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 查询供应商下拉列表
    @PostMapping("/getComboboxList")
    @ResponseBody
    public List<Supplier> getComboboxList(String q){
        return supplierService.getComboboxList(q);
    }
}
