package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.CustomerReturnList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.CustomerReturnListService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/customerReturnListGoods")
public class CustomerReturnListController {

    @Autowired
    private CustomerReturnListService customerReturnListService;

    // 客户退货单保存
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(CustomerReturnList customerReturnList,
                          @Param("customerReturnListGoodsStr") String customerReturnListGoodsStr,
                          HttpServletRequest request){
        // 获取用户id
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("currentUser");
        // 设置用户id
        customerReturnList.setUserId(user.getUserId());
        // 调用service
        customerReturnListService.save(customerReturnList,customerReturnListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 客户退货单查询
    @PostMapping("/list")
    @ResponseBody
    public Map<String, Object> list(@Param("returnNumber") String returnNumber,
                                    @Param("customerId") Integer customerId,
                                    @Param("state") Integer state,
                                    @Param("sTime") String sTime,
                                    @Param("eTime") String eTime) {
        return customerReturnListService.list(returnNumber, customerId, state, sTime, eTime);
    }

    // 退货单商品信息
    @PostMapping("/goodsList")
    @ResponseBody
    public Map<String, Object> goodsList(Integer customerReturnListId){
        return customerReturnListService.goodsList(customerReturnListId);
    }

    // 删除退货单
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(Integer customerReturnListId){
        customerReturnListService.delete(customerReturnListId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 客户退货统计
    @PostMapping("/count")
    @ResponseBody
    public String getCount(@Param("sTime") String sTime,
                           @Param("eTime")String eTime,
                           @Param("goodsTypeId")Integer goodsTypeId,
                           @Param("codeOrName")String codeOrName){
        return customerReturnListService.getCount(sTime,eTime,goodsTypeId,codeOrName);
    }
}
