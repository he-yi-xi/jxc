package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import com.google.gson.JsonObject;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @description 商品
 */
@Controller
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    // 首页商品列表
    @PostMapping("/listInventory")
    @ResponseBody
    public Map<String,Object> listInventory(Integer page,Integer rows,String codeOrName, Integer goodsTypeId){
        return goodsService.listInventory(page,rows,codeOrName,goodsTypeId);
    }

    // 商品列表
    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> list(Integer page,Integer rows,String codeOrName, Integer goodsTypeId){
        return goodsService.listInventory(page,rows,codeOrName,goodsTypeId);
    }

    // 新增或修改商品
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(Goods goods){
        goodsService.save(goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 生成商品编号
    @PostMapping("/getCode")
    @ResponseBody
    public String getCode(){
        // 1.获取所有商品编码
        List<String> listCode = goodsService.getListCode();
        // 2.获取集合最后一个+1
        String number = listCode.get(listCode.size() - 1);
        Integer code = Integer.parseInt(number) +1;
        // 3.返回
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("info","00" + code);
        return jsonObject.toString();
    }

    // 删除商品
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO deleteById(@Param("goodsId")Integer goodsId){
        goodsService.deleteById(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 无库存商品列表
    @PostMapping("/getNoInventoryQuantity")
    @ResponseBody
    public Map<String,Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        return goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
    }

    // 有库存列表
    @PostMapping("/getHasInventoryQuantity")
    @ResponseBody
    public Map<String,Object> getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        return goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
    }

    // 修改库存和成本价
    @PostMapping("/saveStock")
    @ResponseBody
    public ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice){
        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 删除库存
    @PostMapping("/deleteStock")
    @ResponseBody
    public ServiceVO deleteStock(@Param("goodsId")Integer goodsId){
       boolean flag = goodsService.deleteById(goodsId);
       if (flag){
           return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
       }
       return null;
    }

    // 查询所有 当前库存量 小于 库存下限的商品信息
    @PostMapping("/listAlarm")
    @ResponseBody
    public Map<String,Object> getListAlarm(){
        return goodsService.getListAlarm();
    }
}
