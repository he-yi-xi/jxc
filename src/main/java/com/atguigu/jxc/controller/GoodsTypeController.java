package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    // 加载商品类别
    @PostMapping("/loadGoodsType")
    @ResponseBody
    public String loadGoodsType(){
        // 调用service
        String jsonStr = goodsTypeService.loadGoodsType();
        return jsonStr;
    }

    // 新增分类
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(String goodsTypeName,Integer  pId){
        goodsTypeService.save(goodsTypeName,pId);
       return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 删除分类
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(@Param("goodsTypeId")Integer goodsTypeId){
       boolean flag =  goodsTypeService.delete(goodsTypeId);
       if (flag){
           return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
       }else {
           return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE,ErrorCode.GOODS_TYPE_ERROR_MESS);
       }
    }

}
