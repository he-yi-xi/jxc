package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.PurchaseList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.PurchaseListService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/purchaseListGoods")
public class PurchaseListController {

    @Autowired
    private PurchaseListService purchaseListService;

    // 保存进货单
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(PurchaseList purchaseList, String purchaseListGoodsStr, HttpServletRequest request){
        // 获取用户id
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("currentUser");
        // 设置用户id
        purchaseList.setUserId(user.getUserId());
        purchaseListService.save(purchaseList,purchaseListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 进货单列表展示
    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> getPurchaseList(@Param("purchaseNumber") String purchaseNumber,
                                              @Param("supplierId")Integer supplierId,
                                              @Param("state")Integer state,
                                              @Param("sTime")String sTime,
                                              @Param("eTime")String eTime){

        return purchaseListService.getPurchaseList(purchaseNumber,supplierId,state,sTime,eTime);

    }

    // 查询进货单商品信息
    @PostMapping("/goodsList")
    @ResponseBody
    public Map<String,Object> getGoodsList(Integer purchaseListId){
        return purchaseListService.getGoodsList(purchaseListId);
    }

    // 进货单删除
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(Integer purchaseListId){
        purchaseListService.delete(purchaseListId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 支付结算（修改进货单付款状态）
    @PostMapping("/updateState")
    @ResponseBody
    public ServiceVO updateState(Integer purchaseListId){
        purchaseListService.updateState(purchaseListId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    // 进货统计
    @PostMapping("/count")
    @ResponseBody
    public String getCount(@Param("sTime") String sTime,
                           @Param("eTime")String eTime,
                           @Param("goodsTypeId")Integer goodsTypeId,
                           @Param("codeOrName")String codeOrName){
        return purchaseListService.getCount(sTime,eTime,goodsTypeId,codeOrName);
    }



}
