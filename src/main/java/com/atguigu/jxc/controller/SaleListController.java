package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.SaleList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.SaleListService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/saleListGoods")
public class SaleListController {

    @Autowired
    private SaleListService saleListService;

    // 销售单保存
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(SaleList saleList, String saleListGoodsStr, HttpServletRequest request) {
        // 获取用户id
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("currentUser");
        saleList.setUserId(user.getUserId());
        // 保存
        saleListService.save(saleList, saleListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    // 销售单查询
    @PostMapping("/list")
    @ResponseBody
    public Map<String, Object> list(@Param("saleNumber") String saleNumber,
                                    @Param("customerId") Integer customerId,
                                    @Param("state") Integer state,
                                    @Param("sTime") String sTime,
                                    @Param("eTime") String eTime) {
        return saleListService.list(saleNumber, customerId, state, sTime, eTime);
    }

    // 销售单商品信息
    @PostMapping("/goodsList")
    @ResponseBody
    public Map<String, Object> goodsList(Integer saleListId){
        return saleListService.goodsList(saleListId);
    }

    // 删除销售单
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(Integer saleListId){
        saleListService.delete(saleListId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    // 修改销售单付款状态
    @PostMapping("/updateState")
    @ResponseBody
    public ServiceVO updateState(Integer saleListId){
        saleListService.updateState(saleListId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    // 销售统计
    @PostMapping("/count")
    @ResponseBody
    public String getCount(@Param("sTime") String sTime,
                           @Param("eTime")String eTime,
                           @Param("goodsTypeId")Integer goodsTypeId,
                           @Param("codeOrName")String codeOrName){
        return saleListService.getCount(sTime,eTime,goodsTypeId,codeOrName);
    }

    // 按日统计接口
    @PostMapping("/getSaleDataByDay")
    @ResponseBody
    public String getSaleDataByDay(@Param("sTime") String sTime,@Param("eTime") String eTime){
        return saleListService.getSaleDataByDay(sTime,eTime);
    }

    // 按月统计接口
    @PostMapping("/getSaleDataByMonth")
    @ResponseBody
    public String getSaleDataByMonth(@Param("sTime") String sTime,@Param("eTime") String eTime){
        return saleListService.getSaleDataByMonth(sTime,eTime);
    }


}
