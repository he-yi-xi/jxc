package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.ReturnList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.ReturnListService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/returnListGoods")
public class ReturnListController {

    @Autowired
    private ReturnListService returnListService;

    // 退货单保存
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(ReturnList returnList,
                          String returnListGoodsStr,
                          HttpServletRequest request) {

        // 获取用户id
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("currentUser");
        returnList.setUserId(user.getUserId());
        // 调用service
        returnListService.save(returnList,returnListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    // 退货单列表展示
    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> getReturnList(@Param("returnNumber") String returnNumber,
                                            @Param("supplierId")Integer supplierId,
                                            @Param("state")Integer state,
                                            @Param("sTime")String sTime,
                                            @Param("eTime")String eTime){
        return returnListService.getReturnList(returnNumber,supplierId,state,sTime,eTime);
    }

    // 退货单商品信息
    @PostMapping("/goodsList")
    @ResponseBody
    public Map<String,Object> getReturnListGoods(Integer returnListId){
        return returnListService.getReturnListGoods(returnListId);
    }

    // 删除退货单
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(Integer returnListId){
        returnListService.delete(returnListId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    // 退货统计
    @PostMapping("/count")
    @ResponseBody
    public String getCount(@Param("sTime") String sTime,
                           @Param("eTime")String eTime ,
                           @Param("goodsTypeId")Integer goodsTypeId,
                           @Param("codeOrName")String codeOrName){
        return returnListService.getCount(sTime,eTime,goodsTypeId,codeOrName);
    }
}
