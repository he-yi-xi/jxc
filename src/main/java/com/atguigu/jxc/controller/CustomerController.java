package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 用户信息Controller控制器
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    // 客户列表
    @RequestMapping("/list")
    @ResponseBody
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        return customerService.getPageList(page, rows, customerName);
    }

    // 添加客户
    // 添加供应商
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO saveSupplier(Customer customer, HttpServletRequest request) {
        if (customer == null) {
            return new ServiceVO(101, "请求失败");
        }
        // 获取customerId
        String supplierId = request.getParameter("customerId");
        if (StringUtils.isEmpty(supplierId)) {
            customerService.saveSupplier(customer);
        } else {
            // 更新
            customerService.updateSupplier(customer);
        }
        return new ServiceVO(100, "请求成功");
    }

    // 删除客户
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(@Param("ids") String[] ids) {
        System.out.println(ids);
        List<String> stringList = Arrays.asList(ids);
        customerService.delete(stringList);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    // 客户下拉列表
    @PostMapping("/getComboboxList")
    @ResponseBody
    public List<Customer> getComboboxList(String q) {
        return customerService.getComboboxList(q);
    }




}
