package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/overflowListGoods")
public class OverflowListController {

    @Autowired
    private OverflowListService overflowListService;



    // 保存报溢单
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(OverflowList overflowList,
                          @Param("overflowListGoodsStr") String overflowListGoodsStr,
                          HttpServletRequest request){
        // 获取用户id
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());
        // 调用service
        overflowListService.save(overflowList,overflowListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }

    // 报溢单查询
    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> getOverflowList(@Param("sTime") String sTime, @Param("eTime") String eTime){
        return overflowListService.getOverflowList(sTime,eTime);
    }

    // 查询报溢单商品信息
    @PostMapping("/goodsList")
    @ResponseBody
    public Map<String,Object> getGoodsList(Integer overflowListId){
        return overflowListService.getGoodsList(overflowListId);
    }

}
