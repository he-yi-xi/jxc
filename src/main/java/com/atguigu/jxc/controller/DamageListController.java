package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/damageListGoods")
public class DamageListController {

    @Autowired
    private DamageListService damageListService;

    // 保存报损单
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(DamageList damageList,
                          @Param("damageListGoodsStr") String damageListGoodsStr,
                          HttpServletRequest request){
        // 获取用户id
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("currentUser");
        damageList.setUserId(user.getUserId());
        // 调用service
        damageListService.save(damageList,damageListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }

    // 报损单查询
    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> getDamageList(@Param("sTime") String sTime,@Param("eTime") String eTime){
        return damageListService.getDamageList(sTime,eTime);
    }

    // 查询报损单商品信息
    @PostMapping("/goodsList")
    @ResponseBody
    public Map<String,Object> getGoodsList(Integer damageListId){
        return damageListService.getGoodsList(damageListId);
    }

}
