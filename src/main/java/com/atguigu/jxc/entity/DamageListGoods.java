package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 报损商品
 */
@Data
public class DamageListGoods {

  private Integer damageListGoodsId;// 商品报损单商品列表id，主键
  private Integer goodsId;// 商品编号id
  private String goodsCode;// 商品编码
  private String goodsName;// 商品名称
  private String goodsModel;// 商品型号
  private String goodsUnit;// 商品单位
  private Integer goodsNum;// 报损数量
  private double price;// 商品单价
  private double total;// 总金额
  private Integer damageListId;// 商品报损单id
  private Integer goodsTypeId;// 商品类别id

}
