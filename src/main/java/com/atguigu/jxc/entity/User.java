package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 用户实体
 */
@Data
public class User {

  private Integer userId;// 用户id
  private String userName;// 用户名
  private String password;// 密码
  private String trueName;// 真实姓名
  private String roles;// 角色
  private String remarks;// 备注

}
