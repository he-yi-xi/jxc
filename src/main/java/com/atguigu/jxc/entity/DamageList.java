package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 报损
 */
@Data
public class DamageList {

  private Integer damageListId;// 商品报损单id
  private String damageNumber;// 商品报损单号
  private String damageDate;// 创建日期
  private String remarks;// 备注
  private Integer userId;// 用户id

  private String trueName;

}
