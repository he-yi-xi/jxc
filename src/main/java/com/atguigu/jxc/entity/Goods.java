package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 商品信息实体
 */
@Data
public class Goods {

  private Integer goodsId;// 主键id
  private String goodsCode;// 商品编码
  private String goodsName;// 商品名称
  private Integer inventoryQuantity;// 库存数量
  private double lastPurchasingPrice;// 上一次采购价格
  private Integer minNum;// 库存下限
  private String goodsModel;// 商品型号
  private String goodsProducer;// 生产厂商
  private double purchasingPrice;// 采购价格
  private String remarks;// 备注
  private double sellingPrice;// 出售价格
  private Integer state;// 0 初始化状态 1 期初库存入仓库  2  有进货或者销售单据
  private String goodsUnit;// 商品单位
  private Integer goodsTypeId;// 商品类别Id

  private String goodsTypeName;// 商品类别名称

  private Integer saleTotal;// 销售总量

}
