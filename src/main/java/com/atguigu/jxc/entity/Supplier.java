package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 供应商
 */
@Data
public class Supplier {
  // 供应商id，主键
  private Integer supplierId;
  // 供应商名称
  private String supplierName;
  // 联系人
  private String contacts;
  // 联系人电话
  private String phoneNumber;
  // 供应商地址
  private String address;
  // 备注
  private String remarks;

}
