package com.atguigu.jxc.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 商品类别
 */
@Data
@NoArgsConstructor
public class GoodsType {
  //商品类别id
  private Integer goodsTypeId;
  // 商品类别名称
  private String goodsTypeName;
  // 父商品类别id
  private Integer pId;
  // 类别状态,有子级为1，没有子级为0
  private Integer goodsTypeState;

  public GoodsType(String goodsTypeName, Integer goodsTypeState, Integer pId) {
    this.goodsTypeName = goodsTypeName;
    this.goodsTypeState = goodsTypeState;
    this.pId = pId;
  }

}
