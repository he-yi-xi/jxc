package com.atguigu.jxc.entity;

import lombok.Data;

/**
 * 客户实体
 */
@Data
public class Customer {

  private Integer customerId;// 客户编号id
  private String customerName;// 客户名称
  private String contacts;// 联系人
  private String phoneNumber;// 联系人电话
  private String address;// 客户地址
  private String remarks;// 备注

}
