package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import java.util.Map;

public interface DamageListService {

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    void save(DamageList damageList, String damageListGoodsStr);

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> getDamageList(String sTime, String eTime);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    Map<String, Object> getGoodsList(Integer damageListId);
}
