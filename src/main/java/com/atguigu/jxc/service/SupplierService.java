package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

public interface SupplierService {

    /**分页查询供应商信息
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName);

    /**
     * 保存供应商信息
     * @param supplier
     */
    void saveSupplier(Supplier supplier);

    /**
     * 更新供应商信息
     * @param supplier
     */
    void updateSupplier(Supplier supplier);

    /**
     * 删除供应商信息
     * @param ids
     */
    void delete(List<String> ids);

    /**
     * 查询供应山下拉列表
     * @param q
     * @return
     */
    List<Supplier> getComboboxList(String q);
}
