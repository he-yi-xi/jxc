package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.PurchaseList;

import java.util.Map;

public interface PurchaseListService {

    /**
     * 保存进货单
     * @param purchaseList
     * @param purchaseListGoodsStr
     */
    void save(PurchaseList purchaseList, String purchaseListGoodsStr);

    /**
     * 获取进货单列表
     * @param purchaseNumber
     * @param supplierId
     * @param state
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> getPurchaseList(String purchaseNumber, Integer supplierId, Integer state, String sTime, String eTime);

    /**
     * 查询进货单商品信息
     * @param purchaseListId
     * @return
     */
    Map<String, Object> getGoodsList(Integer purchaseListId);

    /**
     * 进货单删除
     * @param purchaseListId
     */
    void delete(Integer purchaseListId);

    /**
     * 修改进货单付款状态
     * @param purchaseListId
     */
    void updateState(Integer purchaseListId);

    /**
     * 进货统计
     * @param sTime
     * @param eTime
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    String getCount(String sTime, String eTime, Integer goodsTypeId, String codeOrName);
}
