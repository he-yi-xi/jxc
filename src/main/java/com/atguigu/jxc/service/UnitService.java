package com.atguigu.jxc.service;

import java.util.Map;

public interface UnitService {
    // 查询所有单位信息
    Map<String, Object> list();
}
