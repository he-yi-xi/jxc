package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

public interface OverflowListService {


    /**
     * 保存报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     */
    void save(OverflowList overflowList, String overflowListGoodsStr);

    /**
     * 查询报溢单
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> getOverflowList(String sTime, String eTime);

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    Map<String, Object> getGoodsList(Integer overflowListId);
}
