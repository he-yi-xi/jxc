package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.CustomerReturnList;

import java.util.Map;

public interface CustomerReturnListService {

    /**
     * 客户退货单保存
     * @param customerReturnList
     * @param customerReturnListGoodsStr
     */
    void save(CustomerReturnList customerReturnList, String customerReturnListGoodsStr);

    /**
     * 客户退货单查询
     * @param returnNumber
     * @param customerId
     * @param state
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> list(String returnNumber, Integer customerId, Integer state, String sTime, String eTime);

    /**
     * 退货单商品信息
     * @param customerReturnListId
     * @return
     */
    Map<String, Object> goodsList(Integer customerReturnListId);

    /**
     * 删除退货单
     * @param customerReturnListId
     */
    void delete(Integer customerReturnListId);

    /**
     * 客户退货统计
     * @param sTime
     * @param eTime
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    String getCount(String sTime, String eTime, Integer goodsTypeId, String codeOrName);
}
