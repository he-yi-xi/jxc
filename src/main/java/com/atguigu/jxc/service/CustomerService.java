package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.List;
import java.util.Map;

public interface CustomerService {

    /**
     * 客户分页列表
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> getPageList(Integer page, Integer rows, String customerName);

    /**
     * 客户添加
     * @param customer
     */
    void saveSupplier(Customer customer);

    /**
     * 更新客户信息
     * @param customer
     */
    void updateSupplier(Customer customer);

    /**
     * 删除客户
     * @param stringList
     */
    void delete(List<String> stringList);

    /**
     * 客户下拉列表
     * @param q
     * @return
     */
    List<Customer> getComboboxList(String q);


}
