package com.atguigu.jxc.service;


import com.atguigu.jxc.entity.Goods;

import java.util.List;
import java.util.Map;

public interface GoodsService {

    /**
     * 带条件分页查询商品列表
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 新增或修改
     * @param goods
     */
    void save(Goods goods);

    /**
     * 获取商品编码集合
     * @return
     */
    List<String> getListCode();

    /**
     * 删除
     * @param goodsId
     */
    void delete(Integer goodsId);

    /**
     * 无库存商品列表
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 有库存列表
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 修改库存和成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    /**
     * 删除商品
     * @param goodsId
     */
    boolean deleteById(Integer goodsId);

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    Map<String, Object> getListAlarm();
}
