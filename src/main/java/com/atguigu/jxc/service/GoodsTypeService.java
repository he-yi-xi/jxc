package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.GoodsType;

public interface GoodsTypeService {

    /**
     * 获取商品类别
     * @return
     */
    String loadGoodsType();


    /**
     * 新增商品类别
     * @param
     */
    void save(String goodsTypeName, Integer pId);

    /**
     * 删除分类
     * @param goodsTypeId
     */
    boolean delete(Integer goodsTypeId);
}
