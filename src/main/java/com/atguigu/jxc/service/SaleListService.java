package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.SaleList;

import java.util.Map;

public interface SaleListService {

    /**
     * 销售单保存
     * @param saleList
     * @param saleListGoodsStr
     */
    void save(SaleList saleList, String saleListGoodsStr);

    /**
     * 销售单查询
     * @param saleNumber
     * @param customerId
     * @param state
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> list(String saleNumber, Integer customerId, Integer state, String sTime, String eTime);

    /**
     * 销售单商品信息
     * @param saleListId
     * @return
     */
    Map<String, Object> goodsList(Integer saleListId);

    /**
     * 删除销售单
     * @param saleListId
     */
    void delete(Integer saleListId);

    /**
     * 修改销售单付款状态
     * @param saleListId
     */
    void updateState(Integer saleListId);

    /**
     * 销售统计
     * @param sTime
     * @param eTime
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    String getCount(String sTime, String eTime, Integer goodsTypeId, String codeOrName);

    /**
     * 按日统计接口
     * @param sTime
     * @param eTime
     * @return
     */
    String getSaleDataByDay(String sTime, String eTime);

    /**
     * 按月统计接口
     * @param sTime
     * @param eTime
     * @return
     */
    String getSaleDataByMonth(String sTime, String eTime);
}
