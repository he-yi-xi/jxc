package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.PurchaseListDao;
import com.atguigu.jxc.dao.PurchaseListGoodsDao;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.PurchaseList;
import com.atguigu.jxc.entity.PurchaseListGoods;
import com.atguigu.jxc.service.PurchaseListService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PurchaseListServiceImpl implements PurchaseListService {

    @Autowired
    private PurchaseListDao purchaseListDao;
    @Autowired
    private PurchaseListGoodsDao purchaseListGoodsDao;
    @Autowired
    private GoodsDao goodsDao;

    /**
     * 保存进货单
     * @param purchaseList
     * @param purchaseListGoodsStr
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(PurchaseList purchaseList, String purchaseListGoodsStr) {
        // 1.保存进货单
        purchaseList.setState(0);
        purchaseListDao.save(purchaseList);
        // 2.保存进货商品信息
            // 获取DamageListGoods对象
            List<PurchaseListGoods> purchaseListGoods = new ArrayList<>();
            Gson gson = new Gson();
            purchaseListGoods = (List<PurchaseListGoods>) gson.fromJson(purchaseListGoodsStr, new TypeToken<List<PurchaseListGoods>>(){}.getType());
            // 遍历集合
        for (PurchaseListGoods purchaseListGood : purchaseListGoods) {
            // 设置进货单id
            purchaseListGood.setPurchaseListId(purchaseList.getPurchaseListId());
            // 保存进货商品信息
            purchaseListGoodsDao.save(purchaseListGood);
            // 更新该商品库存量、状态、采购价格
            Goods goods = goodsDao.selectGoodsById(purchaseListGood.getGoodsId());
            // 设置该商品的上次采购价格
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            // 设置该商品本次采购价格
            goods.setPurchasingPrice(purchaseListGood.getPrice());
            // 设置该商品的库存量
            goods.setInventoryQuantity(purchaseListGood.getGoodsNum() + goods.getInventoryQuantity());
            // 设置该商品的状态
            goods.setState(2);
            // 更新
            goodsDao.updateStock(goods);
        }

    }

    /**
     * 获取进货单列表
     * @param purchaseNumber
     * @param supplierId
     * @param state
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getPurchaseList(String purchaseNumber, Integer supplierId, Integer state, String sTime, String eTime) {
        // 1.查询
       List<PurchaseList> purchaseLists =  purchaseListDao.getPurchaseList(purchaseNumber,supplierId,state,sTime,eTime);
        // 2.
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",purchaseLists);
        return map;
    }

    /**
     * 查询进货单商品信息
     * @param purchaseListId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer purchaseListId) {
      List<PurchaseListGoods> purchaseListGoods =   purchaseListGoodsDao.selectGoodsList(purchaseListId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",purchaseListGoods);
        return map;
    }

    /**
     * 进货单删除
     * @param purchaseListId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer purchaseListId) {
        // 2.删除进货单商品信息
        purchaseListGoodsDao.deleteById(purchaseListId);
        // 1.删除进货单
        purchaseListDao.deleteById(purchaseListId);

    }

    /**
     * 修改进货单付款状态
     * @param purchaseListId
     */
    @Override
    public void updateState(Integer purchaseListId) {
        PurchaseList purchaseList = purchaseListDao.selectById(purchaseListId);
        purchaseList.setState(purchaseList.getState() == 1?0:1);
        purchaseListDao.updateState(purchaseList);
    }

    /**
     * 进货统计
     * @param sTime
     * @param eTime
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    @Override
    public String getCount(String sTime, String eTime, Integer goodsTypeId, String codeOrName) {
        // 1.创建jsonArray封装结果
        JsonArray jsonArray = new JsonArray();

        // 2.查询所有进货信息
        List<Map<String,Object>> mapList = purchaseListDao.getCount(sTime,eTime,goodsTypeId,codeOrName);
        // 遍历集合
        for (Map<String, Object> map : mapList) {
            // 创建jsonobject
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("number",map.get("number").toString());
            jsonObject.addProperty("date",map.get("date").toString());
            jsonObject.addProperty("supplierName",map.get("supplierName").toString());
            jsonObject.addProperty("code",map.get("code").toString());
            jsonObject.addProperty("name",map.get("name").toString());
            jsonObject.addProperty("model",map.get("model").toString());
            jsonObject.addProperty("goodsType",map.get("goodsType").toString());
            jsonObject.addProperty("unit",map.get("unit").toString());
            jsonObject.addProperty("price",map.get("price").toString());
            jsonObject.addProperty("num",map.get("num").toString());
            jsonObject.addProperty("total",map.get("total").toString());
            //
            jsonArray.add(jsonObject);
        }

        // 3.遍历封装信息

        return jsonArray.toString();
    }
}
