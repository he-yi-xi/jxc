package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListServiceImpl implements OverflowListService {

    @Autowired
    private OverflowListDao overflowListDao;
    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    /**
     * 保存报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     */
    @Override
    public void save(OverflowList overflowList, String overflowListGoodsStr) {
        // 1.保存报溢单
        overflowListDao.save(overflowList);
        // 2.保存报溢单商品信息
        // 2.1获取OverflowList对象
        List<OverflowListGoods> list = new ArrayList<OverflowListGoods>();
        Gson gson = new Gson();
        list = (List<OverflowListGoods>) gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>(){}.getType());
        OverflowListGoods overflowListGoods = list.get(0);
        // 设置报溢单Id
        overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
        // 3.保存报溢商品信息
        overflowListGoodsDao.save(overflowListGoods);
    }

    /**
     * 查询报溢单
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        // 查询
        List<OverflowList> overflowLists = overflowListDao.getOverflowList(sTime,eTime);
        //
        map.put("rows",overflowLists);
        return map;
    }

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer overflowListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowListGoods> listGoods = overflowListGoodsDao.getGoodsList(overflowListId);
        map.put("rows",listGoods);
        return map;
    }


}
