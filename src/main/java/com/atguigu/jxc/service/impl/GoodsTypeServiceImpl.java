package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.vo.GoodsTypeVo;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private GoodsTypeDao goodsTypeDao;
    @Autowired
    private GoodsDao goodsDao;

    /**
     * 获取商品类别
     * @return
     */
    @Override
    public String loadGoodsType() {
        // 1.1创建封装结果数组
        JsonArray jsonArray = new JsonArray();
        // 1.2创建封装结果对象
        JsonObject result = new JsonObject();
        // 2.查询树顶对象
        GoodsType goodsType01 = goodsTypeDao.selectByPid(-1);
        result.addProperty("id", goodsType01.getGoodsTypeId());
        result.addProperty("text", goodsType01.getGoodsTypeName());
        result.addProperty("state", goodsType01.getGoodsTypeState().intValue() == 0?"open":"closed");
        result.addProperty("iconCls","goods-type");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("state",1);
        // jsonObject.
        // result.add("attributes",map);
        result.add("attributes",jsonObject);

        // 加载子级
        JsonArray array = this.loadSubTreeByPid(goodsType01);
        // 设置一级类别的children
        result.add("children",array);
        jsonArray.add(result);
        return jsonArray.toString();
    }


    @Override
    public void save(String goodsTypeName, Integer pId) {
        GoodsType goodsType = new GoodsType();
        goodsType.setGoodsTypeName(goodsTypeName);
        goodsType.setPId(pId);
        if (pId == 1){
            goodsType.setGoodsTypeState(1);
        }else {
            goodsType.setGoodsTypeState(0);
        }
        goodsTypeDao.save(goodsType);
    }

    @Override
    public boolean delete(Integer goodsTypeId) {
        // 如果该分类下有商品，则不能删除
        // 根据id查询是否有该分类商品
       int count =  goodsDao.selectByGoodTypeId(goodsTypeId);
       if (count == 0){
           // 可以删除
           goodsTypeDao.delete(goodsTypeId);
           return true;
       }else {
           // 不可以删除
           return false;
       }
    }

    /**
     * 加载子级
     * @param goodsType
     * @return
     */
    private JsonArray loadSubTreeByPid(GoodsType goodsType) {
        // 创建封装结果数组
        JsonArray array = new JsonArray();
        // 判断是否有子级
        List<GoodsType> childrenList = goodsTypeDao.selectChildrenList(goodsType.getGoodsTypeId());
        if (!StringUtils.isEmpty(childrenList)){
            // 有子级，遍历子级
            for (GoodsType type : childrenList) {
                // 封装子级json对象
                JsonObject obj = new JsonObject();
                // 赋值
                obj.addProperty("id",type.getGoodsTypeId());
                obj.addProperty("text",type.getGoodsTypeName());
                obj.addProperty("state",type.getGoodsTypeState().intValue() == 0?"open":"closed");
                obj.addProperty("iconCls","goods-type");
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("state",goodsType.getGoodsTypeState());
                obj.add("attributes",jsonObject);
                // 加载子级
                JsonArray jsonArray = this.loadSubTreeByPid(type);
                if (jsonArray != null){
                    obj.add("children",jsonArray);
                }
                // 放入子级数组中
                array.add(obj);
            }
            return array;
        }else {
            return null;
        }
    }



}
