package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.CustomerReturnList;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /**
     * 客户分页列表
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> getPageList(Integer page, Integer rows, String customerName) {
        // 创建封装结果对象
        Map<String,Object> map = new HashMap<>();
        // 设置分页参数
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Customer> customerList = customerDao.selectPage(offSet,rows,customerName);
        // 封装结果
        map.put("total",customerList.size());
        map.put("rows",customerList);
        return map;
    }

    /**
     * 客户添加
     * @param customer
     */
    @Override
    public void saveSupplier(Customer customer) {
        customerDao.save(customer);
    }

    /**
     * 更新客户信息
     * @param customer
     */
    @Override
    public void updateSupplier(Customer customer) {
        customerDao.update(customer);
    }

    @Override
    public void delete(List<String> ids) {
        customerDao.delete(ids);
    }

    /**
     * 客户下拉列表
     * @param q
     * @return
     */
    @Override
    public List<Customer> getComboboxList(String q) {
        List<Customer> customerList = customerDao.getComboboxList(q);
        return customerList;
    }


}
