package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.dao.CustomerReturnListDao;
import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.entity.CustomerReturnList;
import com.atguigu.jxc.entity.CustomerReturnListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.ReturnListGoods;
import com.atguigu.jxc.service.CustomerReturnListService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerReturnListServiceImpl implements CustomerReturnListService {

    @Autowired
    private CustomerReturnListDao customerReturnListDao;
    @Autowired
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;
    @Autowired
    private GoodsDao goodsDao;


    /**
     * 客户退货单保存
     * @param customerReturnList
     * @param customerReturnListGoodsStr
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(CustomerReturnList customerReturnList, String customerReturnListGoodsStr) {
        // 1.客户退货单保存
        customerReturnListDao.save(customerReturnList);
        // 2.客户退货商品信息保存
        List<CustomerReturnListGoods> customerReturnListGoods = new ArrayList<>();
        Gson gson = new Gson();
        customerReturnListGoods = (List<CustomerReturnListGoods>) gson.fromJson(customerReturnListGoodsStr, new TypeToken<List<CustomerReturnListGoods>>(){}.getType());
        // 遍历集合
        for (CustomerReturnListGoods customerReturnListGood : customerReturnListGoods) {
            // 设置客户退货单id
            customerReturnListGood.setCustomerReturnListId(customerReturnList.getCustomerReturnListId());
            // 保存
            customerReturnListGoodsDao.save(customerReturnListGood);
            // 3.增加对用商品库存
            Goods goods = new Goods();
            goods.setGoodsId(customerReturnListGood.getGoodsId());
            goods.setInventoryQuantity(customerReturnListGood.getGoodsNum());
            goodsDao.incrStock(goods);
        }
    }

    /**
     * 客户退货单查询
     * @param returnNumber
     * @param customerId
     * @param state
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> list(String returnNumber, Integer customerId, Integer state, String sTime, String eTime) {
        List<CustomerReturnList> customerReturnLists = customerReturnListDao.list(returnNumber,customerId,state,sTime,eTime);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",customerReturnLists);
        return map;
    }

    /**
     * 退货单商品信息
     * @param customerReturnListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer customerReturnListId) {
        List<CustomerReturnListGoods> customerReturnListGoods = customerReturnListGoodsDao.goodsList(customerReturnListId);

        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",customerReturnListGoods);
        return map;
    }

    /**
     * 删除退货单
     * @param customerReturnListId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer customerReturnListId) {
        customerReturnListGoodsDao.deleteByCustomerReturnListId(customerReturnListId);
        customerReturnListDao.deleteByCustomerReturnListId(customerReturnListId);
    }

    /**
     * 客户退货统计
     * @param sTime
     * @param eTime
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    @Override
    public String getCount(String sTime, String eTime, Integer goodsTypeId, String codeOrName) {
        // 1.创建jsonArray封装结果
        JsonArray jsonArray = new JsonArray();
        // 2.查询
        List<Map<String, Object>> mapList = customerReturnListDao.getCount(sTime,eTime,goodsTypeId,codeOrName);
        // 3.遍历集合
        for (Map<String, Object> map : mapList) {
            // 创建jsonobject
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("number",map.get("number").toString());
            jsonObject.addProperty("date",map.get("date").toString());
            jsonObject.addProperty("customerName",map.get("customerName").toString());
            jsonObject.addProperty("code",map.get("code").toString());
            jsonObject.addProperty("name",map.get("name").toString());
            jsonObject.addProperty("model",map.get("model").toString());
            jsonObject.addProperty("goodsType",map.get("goodsType").toString());
            jsonObject.addProperty("unit",map.get("unit").toString());
            jsonObject.addProperty("price",map.get("price").toString());
            jsonObject.addProperty("num",map.get("num").toString());
            jsonObject.addProperty("total",map.get("total").toString());
            //
            jsonArray.add(jsonObject);
        }
        return jsonArray.toString();
    }
}
