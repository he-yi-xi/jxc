package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.PurchaseListGoodsDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.PurchaseListGoods;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private PurchaseListGoodsDao purchaseListGoodsDao;

    /**
     * 带条件分页查询商品列表
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        // 1.创建一个封装结果的map
        Map<String,Object> map = new HashMap<>();
        // 2.调用mapper方法
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsList(offSet,rows,codeOrName,goodsTypeId);
        // 3.将数据放入map中
        map.put("total",goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    /**
     * 新增或修改
     * @param goods
     */
    @Override
    public void save(Goods goods) {
        if (goods.getGoodsId() != null){
            // 修改
            goodsDao.update(goods);
        }else {
            // 新增
            // 设置上次采购价格
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            // 设置库存数量
            goods.setInventoryQuantity(0);
            // 设置状态
            goods.setState(0);
            goodsDao.save(goods);
             /*   // 查询上次采购价格,如果没有设置为采购价格
            PurchaseListGoods purchaseListGoods = purchaseListGoodsDao.getLastPrice(goods.getGoodsCode());
            if (purchaseListGoods == null){
                // 设置上次采购价格
                goods.setLastPurchasingPrice(goods.getPurchasingPrice());
                // 设置库存数量
                goods.setInventoryQuantity(0);
            }else {
                // 设置上次采购价格
                goods.setLastPurchasingPrice(purchaseListGoods.getPrice());
                // 查询库存数量
                Integer number = purchaseListGoodsDao.getStockNum(goods.getGoodsCode());
                goods.setInventoryQuantity(number);
            }
            // 设置状态
            goods.setState(0);
            goodsDao.save(goods);*/
        }
    }

    /**
     * 获取商品编码集合
     * @return
     */
    @Override
    public List<String> getListCode() {

        return goodsDao.getListCode();
    }

    @Override
    public void delete(Integer goodsId) {
        goodsDao.deleteById(goodsId);
    }

    /**
     * 无库存商品列表
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        // 1.创建map
        HashMap<String, Object> hashMap = new HashMap<>();
        // 设置条件
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        // 2.查询list
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet,rows,nameOrCode);
        // 3.封装结果
        hashMap.put("total",goodsList.size());
        hashMap.put("rows",goodsList);

        return hashMap;
    }

    /**
     * 有库存列表
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        // 1.创建map
        HashMap<String, Object> hashMap = new HashMap<>();
        // 设置条件
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        // 2.查询list
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet,rows,nameOrCode);
        // 3.封装结果
        hashMap.put("total",goodsList.size());
        hashMap.put("rows",goodsList);

        return hashMap;
    }

    /**
     * 修改库存和成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        // 1.封装参数
        Goods goods = new Goods();
        goods.setGoodsId(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);
        // 修改状态
        /*if (inventoryQuantity > 0){
            goods.setState(2);
        }*/
        goodsDao.saveStock(goods);
    }

    /**
     * 删除商品
     * @param goodsId
     */
    @Override
    public boolean deleteById(Integer goodsId) {
        // 1.查询商品的状态
        Integer state = goodsDao.selectGoodsState(goodsId);
        // 2.判断
        if (state == 0){
            goodsDao.deleteById(goodsId);
            return true;
        }
        return false;
    }

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    @Override
    public Map<String, Object> getListAlarm() {
        // 1.创建一个map
        HashMap<String, Object> map = new HashMap<>();
        // 2.查询
       List<Goods> goodsList = goodsDao.getListAlarm();
       map.put("rows",goodsList);
        return map;
    }
}
