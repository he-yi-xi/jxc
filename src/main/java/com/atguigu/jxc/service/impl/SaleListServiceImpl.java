package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.SaleListDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.ReturnListGoods;
import com.atguigu.jxc.entity.SaleList;
import com.atguigu.jxc.entity.SaleListGoods;
import com.atguigu.jxc.service.SaleListService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SaleListServiceImpl implements SaleListService {

    @Autowired
    private SaleListDao saleListDao;
    @Autowired
    private SaleListGoodsDao saleListGoodsDao;
    @Autowired
    private GoodsDao goodsDao;


    /**
     * 销售单保存
     * @param saleList
     * @param saleListGoodsStr
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(SaleList saleList, String saleListGoodsStr) {
        // 1.保存销售单
        saleListDao.save(saleList);
        // 2.保存销售单商品信息
        List<SaleListGoods> saleListGoods = new ArrayList<>();
        Gson gson = new Gson();
        saleListGoods = (List<SaleListGoods>) gson.fromJson(saleListGoodsStr, new TypeToken<List<SaleListGoods>>(){}.getType());
        // 遍历
        for (SaleListGoods saleListGood : saleListGoods) {
            // 设置销售单id
            saleListGood.setSaleListId(saleList.getSaleListId());
            // 保存销售单商品信息
            saleListGoodsDao.save(saleListGood);
            // 3.减少相应商品库存
            Goods goods = new Goods();
            goods.setGoodsId(saleListGood.getGoodsId());
            goods.setInventoryQuantity(saleListGood.getGoodsNum());
            goodsDao.reduceStock(goods);
        }
    }

    /**
     * 销售单查询
     * @param saleNumber
     * @param customerId
     * @param state
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> list(String saleNumber, Integer customerId, Integer state, String sTime, String eTime) {
        List<SaleList> saleLists = saleListDao.list(saleNumber,customerId,state,sTime,eTime);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",saleLists);
        return map;
    }

    /**
     * 销售单商品信息
     * @param saleListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer saleListId) {
       List<SaleListGoods> saleListGoods = saleListGoodsDao.goodsList(saleListId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",saleListGoods);
        return map;
    }

    /**
     * 删除销售单
     * @param saleListId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer saleListId) {
        // 1.删除商品信息
        saleListGoodsDao.deleteBySaleListId(saleListId);
        // 2.删除销售单
        saleListDao.deleteBySaleListId(saleListId);
    }

    /**
     * 修改销售单付款状态
     * @param saleListId
     */
    @Override
    public void updateState(Integer saleListId) {
        SaleList saleList = saleListDao.selectById(saleListId);
        saleList.setState(saleList.getState() == 0?1:0);
        saleListDao.updateState(saleList);
    }

    /**
     * 销售统计
     * @param sTime
     * @param eTime
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    @Override
    public String getCount(String sTime, String eTime, Integer goodsTypeId, String codeOrName) {
        // 1.创建jsonArray封装结果
        JsonArray jsonArray = new JsonArray();
        // 2.查询
        List<Map<String, Object>> mapList = saleListDao.getCount(sTime,eTime,goodsTypeId,codeOrName);
        // 3.遍历集合
        for (Map<String, Object> map : mapList) {
            // 创建jsonobject
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("number",map.get("number").toString());
            jsonObject.addProperty("date",map.get("date").toString());
            jsonObject.addProperty("customerName",map.get("customerName").toString());
            jsonObject.addProperty("code",map.get("code").toString());
            jsonObject.addProperty("name",map.get("name").toString());
            jsonObject.addProperty("model",map.get("model").toString());
            jsonObject.addProperty("goodsType",map.get("goodsType").toString());
            jsonObject.addProperty("unit",map.get("unit").toString());
            jsonObject.addProperty("price",map.get("price").toString());
            jsonObject.addProperty("num",map.get("num").toString());
            jsonObject.addProperty("total",map.get("total").toString());
            //
            jsonArray.add(jsonObject);
        }
        return jsonArray.toString();
    }

    /**
     * 按日统计接口
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public String getSaleDataByDay(String sTime, String eTime) {
        // 1.创建jsonArray封装结果
        JsonArray jsonArray = new JsonArray();

        // 2.
        List<Map<String,Object>> mapList = saleListDao.getSaleDataByDay(sTime,eTime);
        // 遍历集合
        for (Map<String, Object> map : mapList) {
            // 创建jsonobject
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("date",map.get("date").toString());
            jsonObject.addProperty("saleTotal",map.get("saleTotal").toString());
            jsonObject.addProperty("purchasingTotal",map.get("purchasingTotal").toString());
            jsonObject.addProperty("profit",map.get("profit").toString());
            //
            jsonArray.add(jsonObject);
        }
        // 3.遍历封装信息
        return jsonArray.toString();
    }

    /**
     * 按月统计接口
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public String getSaleDataByMonth(String sTime, String eTime) {
        // 1.创建jsonArray封装结果
        JsonArray jsonArray = new JsonArray();

        // 2.
        List<Map<String,Object>> mapList = saleListDao.getSaleDataByMonth(sTime,eTime);
        // 遍历集合
        for (Map<String, Object> map : mapList) {
            // 创建jsonobject
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("date",map.get("date").toString());
            jsonObject.addProperty("saleTotal",map.get("saleTotal").toString());
            jsonObject.addProperty("purchasingTotal",map.get("purchasingTotal").toString());
            jsonObject.addProperty("profit",map.get("profit").toString());
            //
            jsonArray.add(jsonObject);
        }
        // 3.遍历封装信息
        return jsonArray.toString();
    }
}
