package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.ReturnListDao;
import com.atguigu.jxc.dao.ReturnListGoodsDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.PurchaseListGoods;
import com.atguigu.jxc.entity.ReturnList;
import com.atguigu.jxc.entity.ReturnListGoods;
import com.atguigu.jxc.service.ReturnListService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReturnListServiceImpl implements ReturnListService {

    @Autowired
    private ReturnListDao returnListDao;
    @Autowired
    private ReturnListGoodsDao returnListGoodsDao;
    @Autowired
    private GoodsDao goodsDao;


    /**
     * 保存退货单
     *
     * @param returnList
     * @param returnListGoodsStr
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(ReturnList returnList, String returnListGoodsStr) {
        // 1.保存退货单
        returnListDao.save(returnList);
        // 2.保存退货商品信息
        List<ReturnListGoods> returnListGoodsList = new ArrayList<>();
        Gson gson = new Gson();
        returnListGoodsList = (List<ReturnListGoods>) gson.fromJson(returnListGoodsStr, new TypeToken<List<ReturnListGoods>>() {
        }.getType());
        // 遍历集合
        for (ReturnListGoods returnListGoods : returnListGoodsList) {
            // 设置退货单Id
            returnListGoods.setReturnListId(returnList.getReturnListId());
            // 保存退货商品信息
            returnListGoodsDao.save(returnListGoods);
            // 减少对应商品库存量
            Goods goods = new Goods();
            goods.setGoodsId(returnListGoods.getGoodsId());
            goods.setInventoryQuantity(returnListGoods.getGoodsNum());
            goodsDao.reduceStock(goods);
        }

    }

    /**
     * 展现退货单列表
     *
     * @param returnNumber
     * @param supplierId
     * @param state
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getReturnList(String returnNumber, Integer supplierId, Integer state, String sTime, String eTime) {
        List<ReturnList> returnLists = returnListDao.getReturnList(returnNumber, supplierId, state, sTime, eTime);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows", returnLists);
        return map;
    }

    /**
     * 退货单商品信息
     *
     * @param returnListId
     * @return
     */
    @Override
    public Map<String, Object> getReturnListGoods(Integer returnListId) {
        List<ReturnListGoods> returnListGoods = returnListGoodsDao.getReturnListGoods(returnListId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows", returnListGoods);
        return map;
    }

    /**
     * 删除退货单
     *
     * @param returnListId
     */
    @Override
    @Transactional
    public void delete(Integer returnListId) {
        // 1.删除退货商品信息
        returnListGoodsDao.deleteByReturnListId(returnListId);
        // 2.删除退货单
        returnListDao.deleteByReturnListId(returnListId);
    }

    /**
     * 退货统计
     *
     * @param sTime
     * @param eTime
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    @Override
    public String getCount(String sTime, String eTime, Integer goodsTypeId, String codeOrName) {
        // 1.创建jsonArray封装结果
        JsonArray jsonArray = new JsonArray();
        // 2.查询
        List<Map<String, Object>> mapList = returnListDao.getCount(sTime, eTime, goodsTypeId, codeOrName);
        // 3.遍历集合
        for (Map<String, Object> map : mapList) {
            // 创建jsonobject
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("number",map.get("number").toString());
            jsonObject.addProperty("date",map.get("date").toString());
            jsonObject.addProperty("supplierName",map.get("supplierName").toString());
            jsonObject.addProperty("code",map.get("code").toString());
            jsonObject.addProperty("name",map.get("name").toString());
            jsonObject.addProperty("model",map.get("model").toString());
            jsonObject.addProperty("goodsType",map.get("goodsType").toString());
            jsonObject.addProperty("unit",map.get("unit").toString());
            jsonObject.addProperty("price",map.get("price").toString());
            jsonObject.addProperty("num",map.get("num").toString());
            jsonObject.addProperty("total",map.get("total").toString());
            //
            jsonArray.add(jsonObject);
        }
        return jsonArray.toString();
    }
}
