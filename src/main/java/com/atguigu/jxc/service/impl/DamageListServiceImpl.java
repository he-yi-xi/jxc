package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListService;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListServiceImpl implements DamageListService {

    @Autowired
    private DamageListDao damageListDao;
    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(DamageList damageList, String damageListGoodsStr) {

            // 1.保存报损单
            damageListDao.save(damageList);
            // 2.报损商品关联表
            // 2.1获取DamageListGoods对象
            List<DamageListGoods> list = new ArrayList<DamageListGoods>();
            Gson gson = new Gson();
            list = (List<DamageListGoods>) gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>(){}.getType());
            DamageListGoods damageListGoods = list.get(0);
            // 2.2设置商品报损单id
            damageListGoods.setDamageListId(damageList.getDamageListId());
            // 3.保存保存商品信息
            damageListGoodsDao.save(damageListGoods);

    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getDamageList(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        // 查询
        List<DamageList> damageLists = damageListDao.getDamageList(sTime,eTime);
        //
        map.put("rows",damageLists);
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.getGoodsList(damageListId);
        map.put("rows",damageListGoodsList);
        return map;
    }
}
