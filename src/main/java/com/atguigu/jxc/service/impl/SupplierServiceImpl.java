package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    /**分页查询供应商信息
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
        // 1.创建一个封装结果的map
        Map<String,Object> map = new HashMap<>();
        // 2.调用mapper方法
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
            // 调用mapper
          List<Supplier> supplierList =  supplierDao.getSupplierList(offSet,rows,supplierName);
        // 封装map
        map.put("total",supplierList.size());
        map.put("rows",supplierList);
        return map;
    }

    /**
     * 保存供应商信息
     * @param supplier
     */
    @Override
    public void saveSupplier(Supplier supplier) {
        supplierDao.save(supplier);
    }

    /**
     * 更新供应商信息
     * @param supplier
     */
    @Override
    public void updateSupplier(Supplier supplier) {
        supplierDao.updateById(supplier);
    }

    @Override
    public void delete(List<String> ids) {
        supplierDao.delete(ids);
    }

    /**
     * 查询供应山下拉列表
     * @param q
     * @return
     */
    @Override
    public List<Supplier> getComboboxList(String q) {
        List<Supplier> supplierList = new ArrayList<>();
        supplierList = supplierDao.selectComboboxList(q);
        return supplierList;
    }
}
