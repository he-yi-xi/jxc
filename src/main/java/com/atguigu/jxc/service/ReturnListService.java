package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.ReturnList;

import java.util.Map;

public interface ReturnListService {

    /**
     * 保存退货单
     * @param returnList
     * @param returnListGoodsStr
     */
    void save(ReturnList returnList, String returnListGoodsStr);

    /**
     * 展现退货单列表
     * @param returnNumber
     * @param supplierId
     * @param state
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> getReturnList(String returnNumber, Integer supplierId, Integer state, String sTime, String eTime);

    /**
     * 退货单商品信息
     * @param returnListId
     * @return
     */
    Map<String, Object> getReturnListGoods(Integer returnListId);

    /**
     * 删除退货单
     * @param returnListId
     */
    void delete(Integer returnListId);

    /**
     * 退货统计
     * @param sTime
     * @param eTime
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    String getCount(String sTime, String eTime, Integer goodsTypeId, String codeOrName);
}
