package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.PurchaseListGoods;

import java.util.List;

public interface PurchaseListGoodsDao {
    PurchaseListGoods getLastPrice(String goodsCode);

    Integer getStockNum(String goodsCode);
    // 保存进货商品信息
    void save(PurchaseListGoods purchaseListGoods1);
    // 查询进货单商品信息
    List<PurchaseListGoods> selectGoodsList(Integer purchaseListId);
    // 删除进货单商品信息
    void deleteById(Integer purchaseListId);
}
