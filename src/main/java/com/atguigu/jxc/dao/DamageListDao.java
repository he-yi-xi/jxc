package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListDao {
    // 保存商品报损单
    void save(DamageList damageList);
    // 查询报损单
    List<DamageList> getDamageList(@Param("sTime") String sTime,@Param("eTime") String eTime);
}
