package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.PurchaseList;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PurchaseListDao {
    // 保存进货单
    void save(PurchaseList purchaseList);
    //  获取进货单列表
    List<PurchaseList> getPurchaseList(@Param("purchaseNumber") String purchaseNumber,@Param("supplierId") Integer supplierId,@Param("state") Integer state,@Param("sTime") String sTime,@Param("eTime") String eTime);
    // 删除进货单
    void deleteById(Integer purchaseListId);
    // 修改进货单付款状态
    void updateState(PurchaseList purchaseList);
    // 根据id查询
    PurchaseList selectById(Integer purchaseListId);
    // 查询进货统计
    List<Map<String, Object>> getCount(@Param("sTime") String sTime,@Param("eTime") String eTime,@Param("goodsTypeId") Integer goodsTypeId,@Param("codeOrName") String codeOrName);
}
