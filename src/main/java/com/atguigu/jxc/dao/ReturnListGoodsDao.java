package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.ReturnListGoods;

import java.util.List;

public interface ReturnListGoodsDao {
    // 保存退货商品信息
    void save(ReturnListGoods returnListGoods);
    // 查询退货单商品信息
    List<ReturnListGoods> getReturnListGoods(Integer returnListId);
    // 删除退货单商品信息
    void deleteByReturnListId(Integer returnListId);
}
