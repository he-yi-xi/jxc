package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.CustomerReturnList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    // 查询客户信息分页列表
    List<Customer> selectPage(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("customerName") String customerName);
    // 添加客户
    void save(Customer customer);
    // 修改客户信息
    void update(Customer customer);

    void delete(@Param("ids")List<String> ids);
    // 客户下拉列表
    List<Customer> getComboboxList(String q);

}
