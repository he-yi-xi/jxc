package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品
 */
public interface GoodsDao {

    List<Goods> getGoodsList(@Param("offSet") int offSet,@Param("pageRows") Integer pageRows, @Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);
    // 根据分类id查询
    int selectByGoodTypeId(Integer goodsTypeId);
    // 更新
    void update(Goods goods);
    // 保存
    void save(Goods goods);
    // 商品编码集合
    List<String> getListCode();
    // 删除商品
    void deleteById(Integer goodsId);
    // 无库存商品列表
    List<Goods> getNoInventoryQuantity(@Param("offSet")Integer offSet,@Param("pageRows") Integer pageRows,@Param("nameOrCode") String nameOrCode);
    // 有库存商品列表
    List<Goods> getHasInventoryQuantity(@Param("offSet")int offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);
    // 修改库存和成本价
    boolean saveStock(Goods goods);
    // 查询商品状态
    Integer selectGoodsState(Integer goodsId);
    // 查询所有 当前库存量 小于 库存下限的商品信息
    List<Goods> getListAlarm();
    // 更新商品库存和状态
    void updateStock(Goods goods);
    // 根据id查询商品
    Goods selectGoodsById(Integer goodsId);
    // 减少对应商品库存
    void reduceStock(Goods goods);
    // 增加对应商品库存
    void incrStock(Goods goods);
}
