package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverflowListGoodsDao {
    // 保存报溢单商品信息
    void save(OverflowListGoods overflowListGoods);
    // 查询报溢单商品信息
    List<OverflowListGoods> getGoodsList(Integer overflowListId);
}
