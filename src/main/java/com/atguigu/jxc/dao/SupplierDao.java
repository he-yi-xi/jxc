package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {
    // 分页查询供应商信息
    List<Supplier> getSupplierList(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("supplierName") String supplierName);

    // 保存供应商信息
    void save(Supplier supplier);

    // 更新供应商信息
    void updateById(Supplier supplier);

    // 删除供应商信息
    void delete(@Param("ids") List<String> ids);
    // 查询供应山下拉列表
    List<Supplier> selectComboboxList(String q);
}
