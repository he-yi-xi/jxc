package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.GoodsType;

import java.util.List;

public interface GoodsTypeDao {
    // 查询所有商品类别
    List<GoodsType> selectAll();
    // 查询树顶对象
    GoodsType selectByPid(int i);
    // 查询子级
    List<GoodsType> selectChildrenList(Integer goodsTypeId);
    // 新增
    void save(GoodsType goodsType);
    // 删除
    void delete(Integer goodsTypeId);
}
