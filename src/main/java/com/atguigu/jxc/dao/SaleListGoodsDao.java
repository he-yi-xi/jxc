package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.SaleListGoods;

import java.util.List;

public interface SaleListGoodsDao {
    // 保存销售单商品信息
    void save(SaleListGoods saleListGood);
    // 销售单商品信息
    List<SaleListGoods> goodsList(Integer saleListId);
    // 删除销售单商品信息
    void deleteBySaleListId(Integer saleListId);
}
