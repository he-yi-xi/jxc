package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.SaleList;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SaleListDao {
    // 销售单保存
    void save(SaleList saleList);
    // 销售单查询
    List<SaleList> list(@Param("saleNumber") String saleNumber,@Param("customerId") Integer customerId,@Param("state") Integer state,@Param("sTime") String sTime,@Param("eTime") String eTime);
    // 删除销售单
    void deleteBySaleListId(Integer saleListId);
    // 根据id查询
    SaleList selectById(Integer saleListId);
    // 修改销售单付款状态
    void updateState(SaleList saleList);
    // 销售统计
    List<Map<String, Object>> getCount(@Param("sTime") String sTime,@Param("eTime") String eTime,@Param("goodsTypeId") Integer goodsTypeId,@Param("codeOrName") String codeOrName);
    // 按日统计接口
    List<Map<String, Object>> getSaleDataByDay(@Param("sTime") String sTime,@Param("eTime") String eTime);
    // 按月统计接口
    List<Map<String, Object>> getSaleDataByMonth(@Param("sTime")String sTime,@Param("eTime") String eTime);
}
