package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.CustomerReturnListGoods;

import java.util.List;

public interface CustomerReturnListGoodsDao {
    // 保存退货商品信息
    void save(CustomerReturnListGoods customerReturnListGood);
    // 退货单商品信息
    List<CustomerReturnListGoods> goodsList(Integer customerReturnListId);
    // 删除退货单商品信息
    void deleteByCustomerReturnListId(Integer customerReturnListId);
}
