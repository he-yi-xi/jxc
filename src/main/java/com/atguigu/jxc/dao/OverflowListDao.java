package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListDao {
    // 保存报溢单
    void save(OverflowList overflowList);
    // 查询报溢单
    List<OverflowList> getOverflowList(@Param("sTime") String sTime,@Param("eTime") String eTime);
}
