package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.CustomerReturnList;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CustomerReturnListDao {
    // 客户退货单保存
    void save(CustomerReturnList customerReturnList);

    // 客户退货单查询
    List<CustomerReturnList> list(@Param("returnNumber") String returnNumber, @Param("customerId") Integer customerId, @Param("state") Integer state, @Param("sTime") String sTime, @Param("eTime") String eTime);
    // 删除客户退货单
    void deleteByCustomerReturnListId(Integer customerReturnListId);
    // 客户退货统计
    List<Map<String, Object>> getCount(@Param("sTime") String sTime,@Param("eTime") String eTime,@Param("goodsTypeId") Integer goodsTypeId,@Param("codeOrName")  String codeOrName);
}
