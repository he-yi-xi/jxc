package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

public interface DamageListGoodsDao {
    // 保存报损单商品信息
    void save(DamageListGoods damageListGoods);
    // 查询报损单商品信息
    List<DamageListGoods> getGoodsList(Integer damageListId);
}
