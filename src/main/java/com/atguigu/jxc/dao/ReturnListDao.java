package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.ReturnList;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ReturnListDao {
    // 保存退货单
    void save(ReturnList returnList);
    // 查询退货单列表
    List<ReturnList> getReturnList(@Param("returnNumber") String returnNumber,@Param("supplierId") Integer supplierId,@Param("state") Integer state,@Param("sTime") String sTime,@Param("eTime") String eTime);
    // 删除退货单
    void deleteByReturnListId(Integer returnListId);
    // 退货统计
    List<Map<String, Object>> getCount(@Param("sTime") String sTime,@Param("eTime") String eTime,@Param("goodsTypeId") Integer goodsTypeId,@Param("codeOrName") String codeOrName);
}
