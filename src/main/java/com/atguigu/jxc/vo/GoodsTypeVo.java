package com.atguigu.jxc.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class GoodsTypeVo {

    //商品类别id
    private Integer id;
    // 商品类别名称
    private String text;
    // 商品类别状态
    private String state;
    // 商品类别图标
    private String iconCls;
    // 商品类表属性
    private Map<String,Object> attributes;
    // 商品类别子级
    private List<GoodsTypeVo> children;

}
